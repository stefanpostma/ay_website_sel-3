package com.ymor.delivery.scripts;

import nl.ymor.integrations.ymonitor.config.ConfigUtil;
import nl.ymor.integrations.ymonitor.config.data.MetaData;
import nl.ymor.integrations.ymonitor.parser.JsonParser;
import nl.ymor.integrations.ymonitor.result.Measurement;
import nl.ymor.integrations.ymonitor.result.Transaction;
import nl.ymor.logger.LogWriter;
import nl.ymor.request.proxy.ProxyFactory;
import nl.ymor.request.proxy.ProxyType;
import nl.ymor.selenium.browser.ChromeBrowser;
import nl.ymor.selenium.core.Screenshot;
import nl.ymor.selenium.core.SelUtil;
import nl.ymor.selenium.element.ElementMold;
import nl.ymor.selenium.element.ElementSelector;
import nl.ymor.selenium.element.ElementType;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
	private static final Logger log = Logger.getLogger("YmorLibLogger");
	private static long timeout = 60000;
	private static String username;
	private static String secret;
	private static String proxyServer;
	private static String proxyUsername;
	private static String proxyPassword;
	private static Measurement measurement = new Measurement();

	private WebDriver driver;

	private Main() {
		// Setup logging
		LogWriter.setLogLocation(Paths.get("C:\\YmorTools\\Logging\\Ymor_Administration_SEL\\"));
		LogWriter.setup();
		LogWriter.cleanLogs(14);
	}

	public static void main(String[] args) {
		Main script = new Main();

		try {
			// Get script path
			Path configPath = ConfigUtil.getMetaDataFile(args);
			// Merge metadata.json + script args
			MetaData parameters = ConfigUtil.parseMetaDataFile(configPath, args);
			username = parameters.getParameter("username");
			secret = parameters.getParameter("password");
			proxyServer = parameters.getParameter("proxyServer");
			proxyUsername = parameters.getParameter("proxyUsername");
			proxyPassword = parameters.getParameter("proxyPassword");

			script.run();
		} catch (Exception e) {
			log.log(Level.WARNING, "Exception occured while running script.", e);
		} finally {
			try {
				measurement.generateTransactionsFile();
			} catch (IOException e) {
				log.log(Level.WARNING, "Creating transaction file failed");
			}
			measurement.measurementStop();

			log.info(() -> measurement.toString());
			JsonParser.parseResults(measurement, args);
			System.exit(0);
		}
	}

	private void run() throws Exception {

		// Setup proxy. It is encouraged to define parameters in the metadata.json to
		// fill in the arguments
		ProxyFactory proxyManager = new ProxyFactory();
		proxyManager.setProxyType(ProxyType.MANUAL);
		proxyManager.setProxyServer(proxyServer);
		proxyManager.setProxyPort(8080);
		proxyManager.setProxyUsername(proxyUsername);
		proxyManager.setProxyPassword(proxyPassword);

		// Notice that, if you use proxy settings, you give proxyManager as an argument
		// to ChromeBrowser below
		try (ChromeBrowser browser = new ChromeBrowser(proxyManager)) {
			browser.setIncognitoMode();
			driver = browser.startBrowser();

			transaction01();
			transaction02();
		}
	}

	private void transaction01() throws Exception {
		Transaction trans = measurement.addTransaction("transaction01");

		try {
			trans.start();
			driver.get("https://www.ymonitor.nl/");
			ElementSelector.search(driver, ElementType.XPATH, "//*[@name=\"loginform\"]");
			trans.stop();
		} catch (Exception e) {
			trans.logError(e.toString());
			trans.addScreenshotBase64(Screenshot.getScreenshotAsBase64(driver));
			log.severe(e.toString());
			throw e;
		}
	}

	private void transaction02() throws Exception {
		Transaction trans = measurement.addTransaction("transaction02");

		try {
			ElementSelector.search(driver, ElementType.ID, "username").sendKeys(username);
			ElementSelector.search(driver, ElementType.ID, "password").sendKeys(secret);

			trans.start();
			ElementSelector.search(driver, new ElementMold(ElementType.ID, "login-button")).click();
			ElementSelector.search(driver, timeout, ElementType.XPATH, SelUtil.xpathMatchesText("button", "Logout"));
			trans.stop();
		} catch (Exception e) {
			trans.logError(e.toString());
			trans.addScreenshotBase64(Screenshot.getScreenshotAsBase64(driver));
			log.severe(e.toString());
			throw e;
		}
	}
}
